/* Copyright © 2022 Taylor C. Richberger
 * This code is released under the license described in the LICENSE file
 */

#include <jni.h>
#include <dlfcn.h>
#include <lua.h>
#include <lauxlib.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

struct JVMUserData {
    bool live;
    JavaVM *vm;
    JNIEnv *env;
};

static const struct JVMUserData empty_user_data;
static const struct JavaVMOption empty_option;
static const struct JavaVMInitArgs empty_init_args;

static int vm_gc(lua_State *state) {
    struct JVMUserData *user_data = luaL_checkudata(state, 1, "JVMUserData");
    if (user_data->live) {
        (*user_data->vm)->DestroyJavaVM(user_data->vm);
        *user_data = empty_user_data;
    }
    return 0;
}

static int initialize_vm(lua_State *state) {
    struct JavaVMInitArgs init_args = empty_init_args;
    init_args.version = JNI_VERSION_1_8;
    if (lua_gettop(state) > 1) {
        lua_settop(state, 2);
        init_args.nOptions = luaL_len(state, 2);
        luaL_checkstack(state, init_args.nOptions * 2 + 1, NULL);
        // Let lua clean this up when we don't want it anymore.
        init_args.options = lua_newuserdatauv(state, sizeof(struct JavaVMOption) * init_args.nOptions, 0);
        for (jint i = 0; i < init_args.nOptions; ++i) {
            init_args.options[i] = empty_option;
            lua_geti(state, 2, i + 1);
            init_args.options[i].optionString = luaL_tolstring(state, -1, NULL);
        }
    } else {
        lua_settop(state, 1);
    }

    struct JVMUserData *user_data = lua_newuserdatauv(state, sizeof(struct JVMUserData), 0);
    *user_data = empty_user_data;

    if (luaL_newmetatable(state, "JVMUserData")) {
        lua_pushcfunction(state, vm_gc);
        lua_setfield(state, -2, "__gc");
    }

    lua_setmetatable(state, -2);

    jint rc = JNI_CreateJavaVM(&user_data->vm, (void **)&user_data->env, &init_args);
    if (rc != JNI_OK) {
        return luaL_error(state, "Could not create java VM");
    }
    user_data->live = true;

    return 1;
}

extern int luaopen_java(lua_State *state);

int luaopen_java(lua_State *state) {
    lua_pushcfunction(state, initialize_vm);
    return 1;
}
