DEBUG ?= TRUE
LIBS = 
FLAGS += -MMD -MP -Wall -Wextra -fPIC -pthread -std=c11
JAVA_HOME ?= /usr/lib/jvm/java-1.8.0-openjdk
CFLAGS += -I$(JAVA_HOME)/include -I$(JAVA_HOME)/include/linux
LDFLAGS += -shared -L$(JAVA_HOME)/jre/lib/amd64/server -ljvm

ifdef DEBUG
FLAGS += -O0 -ggdb
CFLAGS += -DDEBUG
else
FLAGS += -O2
CFLAGS += -DNDEBUG
LDFLAGS += -s
endif

CC ?= cc

COMPILE = $(CC) $(CFLAGS) $(FLAGS) -c
LINK = $(CC) $(LDFLAGS) $(FLAGS)

SOURCES = src/java.c
OBJECTS =  $(SOURCES:.c=.o)
DEPENDENCIES = $(OBJECTS:.o=.d)

LIBRARY = java.so

.PHONY: all clean

all: $(LIBRARY)
-include $(DEPENDENCIES)
clean:
	-rm -v $(OBJECTS) $(DEPENDENCIES) $(LIBRARY)

$(LIBRARY): $(OBJECTS)
	$(LINK) -o$@ $^

%.o : %.c
	$(COMPILE) -o $@ $<

